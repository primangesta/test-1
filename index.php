<?php

    function checkIndexCloseParenthesis($string, $idx){
        $arr    = str_split($string);
        $count  = 0;
        $result = 0;
        for($i=$idx; $i<count($arr); $i++){

            if($arr[$idx] != '('){
                return 'Parameter is not index position of an open parenthesis "("';
            }

            if($arr[$i] == '('){
                $count++;
            }

            if($arr[$i] == ')'){
                $count--;
            }

            if($count == 0){
                $result = $i;
                break;
            }
        }

        return $result;
    }

    $result = checkIndexCloseParenthesis("a (b c (d e (f) g) h) i (j k)", 2);
    echo $result;
?>